<?php

namespace App\Http\Controllers;

use App\Models\Libros;
use Illuminate\Support\Facades\Validator; // Agrega esta línea
use Illuminate\Http\Request;


class LibrosController extends Controller
{
    public function index()
    {
        $libros = Libros::all();
        return response ()->json($libros);
    }
    public function store(Request $request)
    {
        $rules = [
            'titulo' => 'required|string|min:1|max:100',
            'autor_id' => 'required|numeric',
            'genero' => 'required|string|min:1|max:100',
            'ano_publicacion' => 'required|date',
            'cantidad_disponible' => 'required|numeric'
        ];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $libros = new Libros($request->input());
        $libros->save();
        return response()->json([
            'status' => true,
            'message' => 'Libro creado successfully'
        ],200);
    }
    public function show(Libros $libro)
    {
        //$libro = Libros::find($id);
        return response()->json(['status' => true, 'data' => $libro]);
    }
    public function update(Request $request, $id)
    {
        $rules = [
            'titulo' => 'required|string|min:1|max:100',
            'autor_id' => 'required|numeric',
            'genero' => 'required|string|min:1|max:100',
            'ano_publicacion' => 'required|date',
            'cantidad_disponible' => 'required|numeric'
        
        ];
        $validator = Validator::make($request->input(),$rules);
        $libro = Libros::find($id);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $libro ->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Libro update successfully'
        ],200);
    }
    public function destroy($id)
    {
        $libro = Libros::find($id);
        $libro->delete();
        return response()->json([
            'status' => true,
            'message' => 'Libro deleted successfully'
        ],200);
    }
}
