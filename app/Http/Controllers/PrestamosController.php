<?php

namespace App\Http\Controllers;

use App\Models\Prestamos;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Autores;
use App\Models\Libros;
use Illuminate\Support\Facades\DB;

class PrestamosController extends Controller
{
    public function index()
    {
        $prestamos = Prestamos::select('prestamos.*', 'libros.titulo as libros')
            ->join('libros', 'libros.id', '=', 'prestamos.libro_id')
            ->paginate(10);

        return response()->json($prestamos);
    }

    public function store(Request $request)
    {
        $rules = [
            'libro_id' => 'required|numeric',
            'usuarios_id' => 'required|numeric',
            'fecha_prestamo' => 'required|date',
            'fecha_devolucion' => 'required|date',
            'estado_prestamo' => 'required|string|min:1|max:100'
        ];
        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ], 400);
        }
        $prestamos = new Prestamos($request->input());
        $prestamos->save();
        return response()->json([
            'status' => true,
            'message' => 'Préstamo creado exitosamente'
        ], 200);
    }

    public function show(Prestamos $prestamo)
    {
        //$prestamo = Prestamos::find($id);
        return response()->json(['status' => true, 'data' => $prestamo]);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'libro_id' => 'required|numeric',
            'usuarios_id' => 'required|numeric',
            'fecha_prestamo' => 'required|date',
            'fecha_devolucion' => 'required|date',
            'estado_prestamo' => 'required|string|min:1|max:100'
        ];
        $validator = Validator::make($request->input(), $rules);
        $prestamo = Prestamos::find($id);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ], 400);
        }
        $prestamo->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Préstamo actualizado exitosamente'
        ], 200);
    }

    public function destroy($id)
    {
        $prestamo = Prestamos::find($id);
        $prestamo->delete();
        return response()->json([
            'status' => true,
            'message' => 'Préstamo eliminado exitosamente'
        ], 200);
    }

    public function PrestamosByLibros()
    {
        $prestamos = Prestamos::select(DB::raw('count(prestamos.id) as count, libros.titulo'))
            ->rightJoin('libros', 'libros.id', '=', 'prestamos.libro_id')
            ->groupBy('libros.titulo')
            ->get();
        return response()->json($prestamos);
    }

    public function all()
    {
        $prestamos = Prestamos::select('prestamos.*', 'libros.titulo as libros')
            ->join('libros', 'libros.id', '=', 'prestamos.libro_id')
            ->get();
        return response()->json($prestamos);
    }
}

