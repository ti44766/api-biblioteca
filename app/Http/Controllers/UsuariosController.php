<?php

namespace App\Http\Controllers;

use App\Models\Usuarios;
use Illuminate\Support\Facades\Validator; // Agrega esta línea
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    public function index()
    {
        $usuarios = Usuarios::all();
        return response ()->json($usuarios);
    }
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|string|min:1|max:100',
            'correo' => 'required|email|max:80',
            'telefono' => 'required|numeric',
            'dirreccion' => 'required|string|min:1|max:200',
            'fecha_registro' => 'required|date'
        ];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $usuarios = new Usuarios($request->input());
        $usuarios->save();
        return response()->json([
            'status' => true,
            'message' => 'Usuario creado successfully'
        ],200);
    }
    public function show(Usuarios $usuario)
    {
        //$usuario = Usuarios::find($id);
        return response()->json(['status' => true, 'data' => $usuario]);
    }
    public function update(Request $request,$id)
    {
        $rules = [
            'nombre' => 'required|string|min:1|max:100',
            'correo' => 'required|email',
            'telefono' => 'required|numeric',
            'dirreccion' => 'required|string|min:1|max:200',
            'fecha_registro' => 'required|date'
        
        ];
        $validator = Validator::make($request->input(),$rules);
        $usuario = Usuarios::find($id);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $usuario ->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Usuario update successfully'
        ],200);
    }
    public function destroy($id)
    {
        $usuario = Usuarios::find($id);
        $usuario->delete();
        return response()->json([
            'status' => true,
            'message' => 'Usuario deleted successfully'
        ],200);
    }
}
