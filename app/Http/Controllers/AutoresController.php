<?php

namespace App\Http\Controllers;

use App\Models\Autores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; // Agrega esta línea


class AutoresController extends Controller
{
    public function index()
    {
        $autores = Autores::all();
        return response ()->json($autores);
    }
    public function store(Request $request)
    {
        $rules = [
            'nombre'=> 'required|string|min:1|max:100',
            'nacionalidad' => 'required|string|min:1|max:100',
            'fecha_nacimiento' => 'required|date|date_format:Y-m-d'
        ];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $autores = new Autores($request->input());
        $autores->save();
        return response()->json([
            'status' => true,
            'message' => 'Autor creado successfully'
        ],200);
    }
    public function show($id)
    {
        $autor = Autores::find($id);
        return response()->json(['status' => true, 'data' => $autor]);
    }
    public function update(Request $request, $id)
    {
        $rules = [
            'nombre'=> 'required|string|min:1|max:100',
            'nacionalidad' => 'required|string|min:1|max:100',
            'fecha_nacimiento' => 'required|date|date_format:Y-m-d'
        
        ];
        $validator = Validator::make($request->input(),$rules);
        $autor = Autores::find($id);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $autor ->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Autor update successfully'
        ],200);
    }
    public function destroy($id)
    {
        $autor = Autores::find($id);
        $autor->delete();
        return response()->json([
            'status' => true,
            'message' => 'Autor deleted successfully'
        ],200);
    }
    
}
