<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LibrosController;
use App\Http\Controllers\AutoresController;
use App\Http\Controllers\PrestamosController;
use App\Http\Controllers\UsuariosController;
use App\Http\Controllers\AuthController;
use App\Models\Usuarios;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::post('auth/register',[AuthController::class, 'create']);
Route::post('auth/login',[AuthController::class, 'login']);


Route::middleware(['auth:sanctum'])->group(function () {
    Route::resource('libros',LibrosController::class);
    Route::resource('autores',AutoresController::class);
    Route::resource('prestamos',PrestamosController::class);
    Route::resource('usuarios',UsuariosController::class);
    Route::get('prestamosall',[PrestamosController::class,'all']);
    Route::get('prestamosbylibros',[PrestamosController::class,'PrestamosByLibros']);
    Route::get('auth/logout',[AuthController::class, 'logout']);

});


