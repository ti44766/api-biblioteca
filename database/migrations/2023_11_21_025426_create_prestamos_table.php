<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('prestamos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('libro_id')->constrained('libros')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('usuarios_id')->constrained('usuarios')->onUpdate('cascade')->onDelete('restrict');
            $table->date('fecha_prestamo',8);
            $table->date('fecha_devolucion',8);
            $table->string('estado_prestamo',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('prestamos');
    }
};
