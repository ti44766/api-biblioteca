<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Prestamos>
 */
class PrestamosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'libro_id' => $this->faker->numberBetween(1,100),
            'usuarios_id' => $this->faker->numberBetween(1,100),
            'fecha_prestamo' => $this->faker->dateTimeBetween('-5 years', 'now'),
            'fecha_devolucion' => $this->faker->dateTimeBetween('-5 years', 'now'),
            'estado_prestamo' => $this->faker->word
        ];
    }
}
