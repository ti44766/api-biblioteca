<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Libros>
 */
class LibrosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'titulo' => $this->faker->jobTitle,
            'autor_id' => $this->faker->numberBetween(1,100),
            'genero' => $this->faker->word,
            'ano_publicacion' =>$this->faker->dateTimeBetween('-5 years', 'now'),
            'cantidad_disponible' => $this->faker->numberBetween(1,100)
        ];
    }
}
