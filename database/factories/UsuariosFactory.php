<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Usuarios>
 */
class UsuariosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        // Genera un número de teléfono de 10 dígitos
        $telefono = $this->faker->numerify('##########');

        return [
            'nombre' => $this->faker->name,
            'correo' => $this->faker->email,
            'telefono' => $telefono,
            'dirreccion' => $this->faker->word,
            'fecha_registro' => $this->faker->dateTimeBetween('-5 years', 'now'),
        ];
    }
}
